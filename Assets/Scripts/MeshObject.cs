﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshObject : MonoBehaviour
{   
    public static Mesh mesh;
    public static GameObject meshObject;
    public Material standardMat;

    private static Vector3 meshPosition = new Vector3(-1f, 0f);


    private void Awake()
    {
        mesh = new Mesh();

        //Set index format to suppost up to 4billion vertices in a mesh
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        meshObject = new GameObject("Mesh");
        meshObject.transform.parent = transform;
        meshObject.AddComponent<MeshFilter>();
        meshObject.AddComponent<MeshRenderer>();
        meshObject.GetComponent<Renderer>().material = standardMat;

        meshObject.transform.localPosition = meshPosition;

    }


    public static void DisplayMesh(Texture rgbTex)
    {

        mesh.Clear();

        mesh.SetVertices(MarchingCubes.vertices);
        mesh.SetTriangles(MarchingCubes.indices, 0);
        mesh.SetUVs(0, MarchingCubes.vertexUVs);

        mesh.SetNormals(MarchingCubes.vertexNormals);
       
        //mesh.RecalculateBounds();
        //mesh.RecalculateNormals();

        meshObject.GetComponent<MeshFilter>().mesh = mesh;        
        meshObject.GetComponent<Renderer>().material.mainTexture = rgbTex;

        //meshObject.transform.RotateAround(new Vector3(-0.911f,-0.267f,0.655f), new Vector3(0, 1, 0), 2.3f);

    }

}
