﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TexTest : MonoBehaviour
{

    public Material standardMat;

    private static Mesh mesh;
    private static GameObject meshObject;

    private MeshRenderer mRenderer;

    


    // Start is called before the first frame update
    void Start()
    {
        mesh = new Mesh();

        //Set index format to suppost up to 4billion vertices in a mesh
        mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        meshObject = new GameObject("Texture");
        meshObject.transform.parent = transform;
        meshObject.AddComponent<MeshFilter>();
        meshObject.AddComponent<MeshRenderer>();
        meshObject.GetComponent<Renderer>().material = standardMat;

        mRenderer = meshObject.GetComponent<MeshRenderer>();

    }

    public static void createTexMesh(Texture rgbTex)
    {
        Vector3 v1 = new Vector3(0, 0);
        Vector3 v2 = new Vector3(2, 0);
        Vector3 v3 = new Vector3(0, 2);
        Vector3 v4 = new Vector3(2, 2);

        Vector3 v5 = new Vector3(1, 0);
        Vector3 v6 = new Vector3(1, 1);

        Vector3[] verts = new[] { v1, v2, v3, v4, v5, v6 };

        int[] triangles = new[] { 0, 2, 1, 2, 3, 1 };
        int[] trianglesv2 = new[] { 0, 2, 4, 4, 2, 5, 4, 5, 1, 1, 5, 3 };

        Vector2 uv1 = new Vector2(0, 0);
        Vector2 uv2 = new Vector2(1, 0);
        Vector2 uv3 = new Vector2(0, 1);
        Vector2 uv4 = new Vector2(1, 1);

        Vector2 uv5 = new Vector2(0.5f, 0);
        Vector2 uv6 = new Vector2(0.5f, 1);


        Vector2[] uvs = new[] { uv1, uv2, uv3, uv4, uv5, uv6 };

        correctUVs(uvs); // 1 - u/v

        mesh.vertices = verts;
        mesh.triangles = triangles;
        mesh.uv = uvs;

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();

        meshObject.GetComponent<MeshFilter>().mesh = mesh;
        meshObject.GetComponent<Renderer>().material.mainTexture = rgbTex;
        meshObject.transform.localPosition = new Vector3(-1.901f, -0.197f, 1.58f);


    }

    private static void correctUVs(Vector2[] uvArr)
    {
        for (int i = 0; i < uvArr.Length; i++) 
        {
            uvArr[i] = new Vector2(1 - uvArr[i].x, 1 - uvArr[i].y);
        }
    }

}
